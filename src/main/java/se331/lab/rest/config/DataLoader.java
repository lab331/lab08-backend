package se331.lab.rest.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import se331.lab.rest.entity.Student;
import se331.lab.rest.repository.StudentRepository;

@Component
public class DataLoader implements ApplicationRunner {
    @Autowired
    StudentRepository studentRepository;
    @Override
    public void run(ApplicationArguments args)throws Exception{
        studentRepository.save(Student.builder()
        .studentId("SE-001")
        .name("Me")
        .surname("Again Me")
        .gpa(3.33)
        .image("")
        .penAmount(11)
        .description("")
        .build());
    }
}
